#coding cp1251
import sys
from antlr4 import *
from MiniJavaLexer import MiniJavaLexer
from MiniJavaParser import MiniJavaParser

# проходим по дереву, ищем ошибки, не нашли - значит строка синтаксически корректна
def iscorrect(tree):
    try:
        if (str(type(tree.exception)) != "<class 'NoneType'>"):
            return False
        for i in tree.children:
            if str(type(i)) == "<class 'antlr4.tree.Tree.ErrorNodeImpl'>":
                return False
            vol = iscorrect(i)
            if not vol:
                return False
        return True
    except:
        return True


# отображаем дерево в консоли
def disp_tree(tree, parser):
    s = tree.toStringTree(tree, parser)
    i = 0
    p = 1
    for i in range(len(s)):
        if i == len(s) - 1:
            if s[i] == '(' and s[i + 1] != ' ':
                print('<')
                print('\t' * p, end='')
            elif s[i] == ')' and s[i - 1] != ' ':
                print('>')
                print('\t' * (p), end='')
            else:
                print(s[i], end='')
        else:
            if s[i + 1] == '(' and s[i + 2] != ' ':
                print(s[i])
                print('\t' * p, end='')
                p += 1
            elif s[i + 1] == ')' and s[i] != ' ':
                p -= 1
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i])
                    print('\t' * p, end='')
            else:
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i], end='')


def main():
    # файл с которого считываем
    input = FileStream("test.txt")
    lexer = MiniJavaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = MiniJavaParser(stream)
    tree = parser.goal()
    correct = iscorrect(tree)
    if correct:
        disp_tree(tree, parser)
    else:
        print('error')
    exit(0)


if __name__ == '__main__':
    main()