#coding cp1251
from antlr4 import *
from MiniJavaLexer import MiniJavaLexer
from MiniJavaParser import MiniJavaParser


class Scope:
    def __init__(self, prev, type, name):
        self.parent = prev
        self.children = []
        self.ids = []
        self.name = name
        self.type = type
        self.params = []
        self.pos = 0
        self.clas = ''
        self.funk = False
        self.extend = None

    # геттеры/сеттеры
    def get_parent(self):
        return self.parent

    def set_parent(self, par):
        self.parent = par

    def get_children(self):
        return self.children

    def get_child(self, a):
        return self.children[a]

    def add_child(self, chil):
        self.children.append(chil)

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_type(self):
        return self.type

    def set_type(self, type):
        self.type = type

    def get_pos(self):
        return self.pos

    def set_pos(self, pos):
        self.pos = pos

    def get_ids(self):
        return self.ids

    def add_id(self, a):
        self.ids.append(a)

    def get_id(self, a):
        return self.ids[a]

    def get_param(self):
        return self.params

    def add_param(self, el):
        self.params.append(el)

    def get_clas(self):
        return self.clas

    def set_clas(self, clas):
        self.clas = clas

    def get_funk(self):
        return self.funk

    def set_funk(self, funk):
        self.funk = funk
    #

    def next_child(self):
        nextChild = None
        pos = self.get_pos()
        if (pos >= len(self.get_children())):
            self.get_children().append(nextChild)
        else:
            nextChild = self.get_child(pos)
        pos += 1
        self.set_pos(pos)
        return nextChild

    def lookup_class(self, name):
        for j in self.children:
            if j.get_name() == name and j.get_funk() == False:
                return j
        if self.get_parent():
            return self.get_parent().lookup_class(name)
        return None

    def lookup_method(self, name):
        p = None
        for j in self.children:
            if j.get_name() == name and j.get_funk() == True:
                return j
        t = False
        for j in self.children:
            if j.get_funk() == False:
                for k in j.children:
                    if k.get_name() == name and k.get_funk() == True:
                        return k
        if self.get_parent():
            return self.get_parent().lookup_method(name)
        return None

    def lookup_var(self, name):
        for i in self.ids:
            if i[1] == name:
                for j in self.children:
                    if j.get_name() == i[1]:
                        return None
                return i
        if self.get_parent():
            return self.get_parent().lookup_var(name)
        return None

    def uniq(self, id):
        for i in self.children:
            if i.name == id[1] and i.type == id[0]:
                return False
        return True

    def go_null(self):
        self.pos = 0
        for i in self.children:
            i.go_null()

    def print(self, tab):
        if self.extend:
            print('\t' * tab + self.type + ' ' + self.name + ' extends ' + self.extend)
        else:
            print('\t' * tab + self.type + ' ' + self.name)
        for i in self.children:
            i.print(tab + 1)
        for i in self.ids:
            if self.uniq(i):
                print('\t' * (tab + 1) + i[0] + ' ' + i[1])

isfoo = False

class SymbolTable:
    def __init__(self, root):
        self.root = root
        self.current = root
        self.expressions = []

    # геттеры/сеттеры
    def get_root(self):
        return self.root

    def set_root(self, root):
        self.root = root

    def get_current(self):
        return self.current

    def set_current(self, cur):
        self.current = cur

    def get_expression(self):
        return self.expressions

    def add_expression(self, a):
        self.expressions.append(a)
    #

    def enter_scope(self):
        self.set_current(self.current.next_child())

    def exit_scope(self):
        if self.current:
            self.set_current(self.current.get_parent())

    def put(self, type, name):
        self.get_current().add_id((type, name))

    def lookup_class_method(self, clas, method):
        if clas == 'this':
            clas = self.get_current().get_clas()
        try:
            idd = self.get_current().lookup_var(clas)[0]
        except:
            idd = clas
        cl = self.get_current().lookup_class(idd)
        if not cl:
            return None
        children = cl.get_children()
        for i in children:
            if i.name == method and i.funk == True:
                return i
        if cl.extend:
            classs = self.get_current().lookup_clas(cl.extend)
            children = classs.get_children()
            for i in children:
                if i.name == method and i.funk == True:
                    return i
        return None

    def lookup_var(self, name):
        return self.get_current().lookup_var(name)

    def lookup_class(self, name):
        return self.get_current().lookup_class(name)

    def checking_expr(self, expr):
        global isfoo
        symb = expr.get_symb()
        if symb == '[':
            if expr.get_el() == 'int':
                if self.checking_expr(expr.get_children()[0]) == 'int':
                    return 'int[]'
                else:
                    return None
            else:
                if self.checking_expr(expr.get_children()[0]) == 'int[]' and self.checking_expr(
                                expr.get_children()[1]) == 'int':
                    return 'int'
                return None
        if symb == 'new':
            return expr.get_el()
        if symb == '.':
            isfoo = True
            a = expr.get_children()
            if expr.get_el() == 'length':
                va = self.checking_expr(a[0])
                if va == 'int[]':
                    return 'int'
                else:
                    return None
            meth = self.get_current().lookup_method(expr.get_el()).get_type()
            changed = False
            try:
                sy = a[0].get_symb()
                val = self.checking_expr(a[0].get_children()[0])
                val1 = meth
                if val1 == None or val == None:
                    return None
                if sy == '+' or sy == '-' or sy == '*':
                    if val1 == 'int' and val == 'int':
                        return 'int'
                    return None
                elif sy == '&':
                    if val1 == 'boolean' and val == 'boolean':
                        return 'boolean'
                    else:
                        return None
                elif sy == '<':
                    if val1 == 'int' and val == 'int':
                        return 'boolean'
                    else:
                        return None
                elif sy == '[':
                    if val1 == 'int[]' and val == 'int':
                        return 'int'
                    return None
            except:
                return meth
        if symb:
            a = self.checking_expr(expr.get_children()[0])
            b = self.checking_expr(expr.get_children()[1])
            if a == None or b == None:
                return None
            if symb == '+' or symb == '-' or symb == '*':
                if a == 'int' and b == 'int':
                    return 'int'
                return None
            elif symb == '&':
                if a == 'boolean' and b == 'boolean':
                    return 'boolean'
                else:
                    return None
            elif symb == '<':
                if a == 'int' and b == 'int':
                    return 'boolean'
                else:
                    return None
            elif symb == '[':
                if a == 'int[]' and b == 'int':
                    return 'int'
                return None
        else:
            a = expr.get_children()
            if len(a) == 1:
                va = self.checking_expr(expr.get_children()[0])
                if expr.get_el() == 'length':
                    if va == 'int[]':
                        return 'int'
                    else:
                        return None
                return va
            else:
                el = expr.get_el()
                if el[0] in ['0','1','2','3','4','5','6','7','8','9']:
                    return 'int'
                elif el == 'true' or el == 'false':
                    return 'boolean'
                elif el == 'this':
                    return self.get_current().get_clas()
                else:
                    try:
                        typ = self.get_current().lookup_var(el)
                    except:
                        return expr.get_el()
                    if typ:
                        return typ[0]
                    else:
                        return None

    def go_null(self):
        self.current = self.root
        self.root.go_null()

    def print(self):
        self.root.print(0)


class Expr:
    def __init__(self, root):
        self.root = root
        self.children = []
        self.symb = None
        self.el = None

    def add_child(self, a):
        self.children.append(a)

    def get_children(self):
        return self.children

    def get_last_child(self):
        return self.children[len(self.children) - 1]

    def set_root(self, a):
        self.root = a

    def get_root(self):
        if self.root:
            return self.root
        return self

    def set_symb(self, a):
        self.symb = a

    def get_symb(self):
        return self.symb

    def get_el(self):
        return self.el

    def set_el(self, a):
        self.el = a


def build_expr(s, pos):
    expr = Expr(None)
    num = 0
    while num >= 0:
        if s[pos] == '(' and s[pos + 1] != ' ':
            num += 1
            expr.add_child(Expr(expr))
            expr = expr.get_last_child()
        elif s[pos] == ')' and s[pos - 1] != ' ':
            num -= 1
            expr = expr.get_root()
        elif s[pos] == '+' or s[pos] == '-' or s[pos] == '*' or s[pos] == '<' or s[pos] == '&' or s[pos] == '[' or s[pos] == '.':
            expr.set_symb(s[pos])
        elif not (s[pos] in ['(',')',';','}','{',']',' ',',']) and (s[pos - 1] == ' ' or s[pos - 1] == '('):
            word , pos = get_word(s, pos)
            pos -= 1
            while word[len(word) - 1] == ')':
                word = word[:len(word) - 1]
                pos -= 1
            if word == 'new':
                expr.set_symb(word)
            else:
                expr.set_el(word)
        pos += 1
    return expr, pos


def get_word(s, i):
    start = i
    while s[i] != ' ':
        i += 1
    return s[start:i], i


def BuildSymbolTable(tree, parser):
    global isfoo
    s = tree.toStringTree(tree, parser)
    clas = 'prog'
    table = SymbolTable(Scope(None, 'prog', 'program'))
    i = 1
    checked_expr = 0
    exist = True
    type_is_correct = True
    word = ''
    scobki = [True]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                clas = word
                table.put('class', word)
                table.get_current().add_child(Scope(table.get_current(),'class', word))
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_clas(clas)
                table.enter_scope()
                word, i = get_word(s, i + 1)
                if word == 'extends':
                    word, i = get_word(s, i + 1)
                    table.get_current().extend = word
                scobki.append(True)
            elif word == 'methodDecl':
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                table.put(word1, word2)
                table.get_current().add_child(Scope(table.get_current(), word1, word2))
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_clas(clas)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_funk(True)
                table.enter_scope()
                while word == '(typ':
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    table.get_current().add_param((word, word2))
                    table.put(word, word2)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                scobki.append(True)
            elif word == 'varDecl':
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                    table.put(word1 + '[]', word2)
                else:
                    table.put(word1, word2)
                scobki.append(False)
            elif word == 'statement':
                word1, i = get_word(s, i + 1)
                scobki.append(False)
            elif word == 'expression':
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1[:4] == 'true' or word1[:5] == 'true)' or word1[:5] == 'false' or word1[:6] == 'false)' or word1[:4] == 'this'or  word1[:5] == 'this)' or word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            if val:
                try:
                    table.exit_scope()
                except:
                    return table
            i += 1
        elif s[i] == '.':
            word, i = get_word(s, i + 2)
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    i -= 1
                continue
            else:
                j = i - len(word) - 5
                while s[j] != ' ':
                    j -= 1
                word1, j = get_word(s, j + 1)
                word1 = word1[:len(word1) - 1]
        else:
            i += 1
    return table


def check_exist(tree, parser, table):
    global isfoo
    s = tree.toStringTree(tree, parser)
    clas = 'prog'
    i = 1
    checked_expr = 0
    exist = True
    type_is_correct = True
    word = ''
    scobki = [True]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                clas = word
                table.enter_scope()
                scobki.append(True)
            elif word == 'methodDecl':
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.enter_scope()
                while word == '(typ':
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                scobki.append(True)
            elif word == 'varDecl':
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                scobki.append(False)
            elif word == 'statement':
                word1, i = get_word(s, i + 1)
                if not(word1 == '{' or word1 == 'if' or word1 == 'while' or word1 == 'System.out.println'):
                    res = table.lookup_var(word1)
                    if not res:
                        exist = False
                scobki.append(False)
            elif word == 'expression':
                if i >= checked_expr:
                    expr, checked_expr = build_expr(s, i - len(word) + 1)
                    ex = table.checking_expr(expr)
                    if isfoo:
                        checked_expr = i
                        while s[checked_expr] != '.':
                            checked_expr += 1
                    isfoo = False
                    table.add_expression((i - 11, ex))
                    if not ex:
                        type_is_correct = False
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1[:4] == 'true' or word1[:5] == 'true)' or word1[:5] == 'false' or word1[:6] == 'false)' or word1[:4] == 'this'or  word1[:5] == 'this)' or word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                    if word1 == 'int':
                        continue
                    else:
                        res = table.lookup_class(word1)
                        if not res:
                            exist = False
                            continue
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    res = table.lookup_var(word1)
                    if not res:
                        exist = False
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            if val:
                try:
                    table.exit_scope()
                except:
                    return exist, type_is_correct
            i += 1
        elif s[i] == '.':
            dot = i
            word, i = get_word(s, i + 2)
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    i -= 1
                continue
            else:
                j = dot - 3
                scob = 1
                while scob > 0 and not (scob == 1 and s[j] in ['+', '-', '*', '&', '<']) :
                    if s[j] == ')' and s[j - 1] != ' ':
                        scob += 1
                    if s[j] == '(' and s[j + 1] != ' ':
                        scob -= 1
                    j -= 1
                while s[j] != '.':
                    j += 1
                if j != dot:
                    word2, j = get_word(s, j + 2)
                    j = j - len(word2) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                    res = table.lookup_class_method(word1, word2)
                    word1 = res.get_type()
                else:
                    j = i - len(word) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                res = table.lookup_class_method(word1, word)
                if not res:
                    exist = False
                    continue
        else:
            i += 1
    return exist, type_is_correct


# отображаем дерево в консоли
def disp_tree(tree, parser):
    s = tree.toStringTree(tree, parser)
    i = 0
    p = 1
    for i in range(len(s)):
        if i == len(s) - 1:
            if s[i] == '(' and s[i + 1] != ' ':
                print('<')
                print('\t' * p, end='')
            elif s[i] == ')' and s[i - 1] != ' ':
                print('>')
                print('\t' * (p), end='')
            else:
                print(s[i], end='')
        else:
            if s[i + 1] == '(' and s[i + 2] != ' ':
                print(s[i])
                print('\t' * p, end='')
                p += 1
            elif s[i + 1] == ')' and s[i] != ' ':
                p -= 1
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i])
                    print('\t' * p, end='')
            else:
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i], end='')

# проходим по дереву, ищем ошибки, не нашли - значит строка синтаксически корректна
def iscorrect(tree):
    try:
        if (str(type(tree.exception)) != "<class 'NoneType'>"):
            return False
        for i in tree.children:
            if str(type(i)) == "<class 'antlr4.tree.Tree.ErrorNodeImpl'>":
                return False
            vol = iscorrect(i)
            if not vol:
                return False
        return True
    except:
        return True


def checking_stat(tree, parser, table):
    s = tree.toStringTree(tree, parser)
    i = 0
    arr = table.get_expression()
    stat = True
    funk = True
    word = ''
    scobki = [True]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.enter_scope()
                scobki.append(True)
            elif word == 'methodDecl':
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                while word == '(typ':
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                table.enter_scope()
                j = i
                word, j = get_word(s, j + 1)
                while word != 'return':
                    word, j = get_word(s, j + 1)
                for k in table.get_expression():
                    if k[0] == j + 1 and k[1] != word1:
                        funk = False
                scobki.append(True)
            elif word == 'varDecl':
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                scobki.append(False)
            elif word == 'statement':
                scobki.append(False)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                    i -= 1
                if word1 == 'if' or word1 == 'while':
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'boolean'):
                            stat = False
                elif word1 == 'System.out.println':
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'int'):
                            stat = False
                elif word1 == '{':
                    continue
                elif s[i + 1] == '=':
                    typ = table.lookup_var(word1)
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != typ[0]):
                            stat = False
                elif s[i + 1] == '[':
                    typ = table.lookup_var(word1)
                    if typ[0] != 'int[]':
                        stat = False
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'int'):
                            stat = False
                    while s[i] != '=':
                        i += 1
                    for j in arr:
                        if (j[0] == i + 2 and j[1] != 'int'):
                            stat = False
            elif word == 'expression':
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1[:4] == 'true' or word1[:5] == 'true)' or word1[:5] == 'false' or word1[
                                                                                            :6] == 'false)' or word1[
                                                                                                               :4] == 'this' or word1[
                                                                                                                                :5] == 'this)' or \
                                word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or \
                                word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[
                    0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                    if word1 == 'int':
                        continue
                    else:
                        res = table.lookup_class(word1)
                        if not res:
                            exist = False
                            continue
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            if val:
                try:
                    table.exit_scope()
                except:
                    return stat, funk
            i += 1
        elif s[i] == '.':
            word, i = get_word(s, i + 2)
            while word[len(word) - 1] == ')':
                word = word[:len(word) - 1]
                i -= 1
            continue
        else:
            i += 1
    return stat, funk


def checking_funk(tree, parser, table):
    s = tree.toStringTree(tree, parser)
    i = 0
    word1 = ''
    word2 = ''
    correct = True
    scobki = [True]
    while i < len(s):
        word1 = word2
        try:
            word2, i = get_word(s, i + 1)
        except:
            return correct
        if word2 in ['(mainClass', '(classDecl', '(methodDecl']:
            scobki.append(True)
            table.enter_scope()
        elif len(word2) > 1 and word2[0] == '(':
            scobki.append(False)
        while word2 != ')' and word2[len(word2) - 1] == ')':
            word2 = word2[:len(word2) - 1]
            p = scobki.pop(len(scobki) - 1)
            if p:
                table.exit_scope()
        if word2 == '.':
            dot = i
            word, i = get_word(s, i + 1)
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    scobki.pop(len(scobki) - 1)
                continue
            else:
                j = dot - 4
                scob = 1
                while scob > 0 and not (scob == 1 and s[j] in ['+', '-', '*', '&', '<']):
                    if s[j] == ')' and s[j - 1] != ' ':
                        scob += 1
                    if s[j] == '(' and s[j + 1] != ' ':
                        scob -= 1
                    j -= 1
                while s[j] != '.':
                    j += 1
                if j != dot - 1:
                    word2, j = get_word(s, j + 2)
                    j = j - len(word2) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                    res = table.lookup_class_method(word1, word2)
                    word1 = res.get_type()
                else:
                    j = i - len(word) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                el = table.lookup_class_method(word1, word)
                can = 0
                j = dot - 1
                while s[j] != '(':
                    j += 1
                num = 1
                while num > 0:
                    j += 1
                    if s[j] == ')':
                        num -= 1
                        if num == 1:
                            can += 1
                    elif s[j] == '(':
                        num += 1
                if can != len(el.get_param()):
                    return False
                pos = 3
                for j in el.get_param():
                    found = False
                    for k in table.get_expression():
                        if i + pos == k[0]:
                            found = True
                            pos += 1
                            val = 1
                            while val > 0:
                                if s[i + pos] == '(':
                                    val += 1
                                elif s[i + pos] == ')':
                                    val -= 1
                                pos += 1
                            pos += 3
                            if j[0] != k[1]:
                                return False
                            break
                    if not found:
                        return False
    return correct


def semantic(tree, parser):
    table = BuildSymbolTable(tree, parser)
    print('\nв таблице есть следующие записи:')
    table.print()
    print('')
    table.go_null()
    exist, type_correct = check_exist(tree, parser, table)
    if exist:
        print('все вызываемые объекты предварительно объявлены')
    else:
        print('ошибка, не все вызываемые объекты предварительно объявлены')
        return
    if type_correct:
        print('все выражения expression корректны')
    else:
        print('ошибка, не все выражения expression корректны')
        return
    table.go_null()
    correct_stat, funk_return = checking_stat(tree, parser, table)
    if correct_stat:
        print('типизация statements корректна')
    else:
        print('ошибка в типизации statements')
        return
    if funk_return:
        print('возвращаемые значения функций корректны')
    else:
        print('ошибка типа возвращаемого значения')
        return
    table.go_null()
    funk_param = checking_funk(tree, parser, table)
    if funk_param:
        print('параметры в функциях корректны')
    else:
        print('ошибка в параметрах вызываемых функций')
    return table
    

def main():
    input = FileStream("test.txt")
    # построение дерева, проверка корректности строки
    lexer = MiniJavaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = MiniJavaParser(stream)
    tree = parser.goal()
    correct = iscorrect(tree)
    if not correct:
        print('ошибка синтаксиса')
    else:
        print('синтакс корректен')
        #disp_tree(tree, parser)
        global tabl
        tabl = semantic(tree, parser)
    exit(0)


if __name__ == '__main__':
    main()