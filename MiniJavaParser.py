# Generated from E:\gram\MiniJava.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3(")
        buf.write("\u00e5\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\3\2\3\2\7\2\25\n\2\f\2\16\2\30\13\2\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4\60\n\4\3\4\3\4\7")
        buf.write("\4\64\n\4\f\4\16\4\67\13\4\3\4\7\4:\n\4\f\4\16\4=\13\4")
        buf.write("\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5G\n\5\3\6\3\6\3\6")
        buf.write("\3\6\3\7\3\7\5\7O\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\7\7Z\n\7\f\7\16\7]\13\7\5\7_\n\7\3\7\3\7\3\7\7\7")
        buf.write("d\n\7\f\7\16\7g\13\7\3\7\7\7j\n\7\f\7\16\7m\13\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\b\3\b\7\bv\n\b\f\b\16\by\13\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a4")
        buf.write("\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00bc\n\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\7\t\u00d1\n\t\f\t\16\t\u00d4\13\t")
        buf.write("\5\t\u00d6\n\t\3\t\5\t\u00d9\n\t\3\t\3\t\3\t\3\t\3\t\7")
        buf.write("\t\u00e0\n\t\f\t\16\t\u00e3\13\t\3\t\2\3\20\n\2\4\6\b")
        buf.write("\n\f\16\20\2\3\3\2\35\36\2\u0100\2\22\3\2\2\2\4\31\3\2")
        buf.write("\2\2\6+\3\2\2\2\bF\3\2\2\2\nH\3\2\2\2\fL\3\2\2\2\16\u00a3")
        buf.write("\3\2\2\2\20\u00bb\3\2\2\2\22\26\5\4\3\2\23\25\5\6\4\2")
        buf.write("\24\23\3\2\2\2\25\30\3\2\2\2\26\24\3\2\2\2\26\27\3\2\2")
        buf.write("\2\27\3\3\2\2\2\30\26\3\2\2\2\31\32\7\3\2\2\32\33\7%\2")
        buf.write("\2\33\34\7\4\2\2\34\35\7\5\2\2\35\36\7\6\2\2\36\37\7\7")
        buf.write("\2\2\37 \7\b\2\2 !\7\t\2\2!\"\7\n\2\2\"#\7\13\2\2#$\7")
        buf.write("\f\2\2$%\7%\2\2%&\7\r\2\2&\'\7\4\2\2\'(\5\16\b\2()\7\16")
        buf.write("\2\2)*\7\16\2\2*\5\3\2\2\2+,\7\3\2\2,/\7%\2\2-.\7\17\2")
        buf.write("\2.\60\7%\2\2/-\3\2\2\2/\60\3\2\2\2\60\61\3\2\2\2\61\65")
        buf.write("\7\4\2\2\62\64\5\n\6\2\63\62\3\2\2\2\64\67\3\2\2\2\65")
        buf.write("\63\3\2\2\2\65\66\3\2\2\2\66;\3\2\2\2\67\65\3\2\2\28:")
        buf.write("\5\f\7\298\3\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<>\3\2")
        buf.write("\2\2=;\3\2\2\2>?\7\16\2\2?\7\3\2\2\2@A\7\20\2\2AB\7\13")
        buf.write("\2\2BG\7\f\2\2CG\7\21\2\2DG\7\20\2\2EG\7%\2\2F@\3\2\2")
        buf.write("\2FC\3\2\2\2FD\3\2\2\2FE\3\2\2\2G\t\3\2\2\2HI\5\b\5\2")
        buf.write("IJ\7%\2\2JK\7\22\2\2K\13\3\2\2\2LN\7\5\2\2MO\7\6\2\2N")
        buf.write("M\3\2\2\2NO\3\2\2\2OP\3\2\2\2PQ\5\b\5\2QR\7%\2\2R^\7\t")
        buf.write("\2\2ST\5\b\5\2T[\7%\2\2UV\7\23\2\2VW\5\b\5\2WX\7%\2\2")
        buf.write("XZ\3\2\2\2YU\3\2\2\2Z]\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\")
        buf.write("_\3\2\2\2][\3\2\2\2^S\3\2\2\2^_\3\2\2\2_`\3\2\2\2`a\7")
        buf.write("\r\2\2ae\7\4\2\2bd\5\n\6\2cb\3\2\2\2dg\3\2\2\2ec\3\2\2")
        buf.write("\2ef\3\2\2\2fk\3\2\2\2ge\3\2\2\2hj\5\16\b\2ih\3\2\2\2")
        buf.write("jm\3\2\2\2ki\3\2\2\2kl\3\2\2\2ln\3\2\2\2mk\3\2\2\2no\7")
        buf.write("\24\2\2op\5\20\t\2pq\7\22\2\2qr\7\16\2\2r\r\3\2\2\2sw")
        buf.write("\7\4\2\2tv\5\16\b\2ut\3\2\2\2vy\3\2\2\2wu\3\2\2\2wx\3")
        buf.write("\2\2\2xz\3\2\2\2yw\3\2\2\2z\u00a4\7\16\2\2{|\7\25\2\2")
        buf.write("|}\7\t\2\2}~\5\20\t\2~\177\7\r\2\2\177\u0080\5\16\b\2")
        buf.write("\u0080\u0081\7\26\2\2\u0081\u0082\5\16\b\2\u0082\u00a4")
        buf.write("\3\2\2\2\u0083\u0084\7\27\2\2\u0084\u0085\7\t\2\2\u0085")
        buf.write("\u0086\5\20\t\2\u0086\u0087\7\r\2\2\u0087\u0088\5\16\b")
        buf.write("\2\u0088\u00a4\3\2\2\2\u0089\u008a\7\30\2\2\u008a\u008b")
        buf.write("\7\t\2\2\u008b\u008c\5\20\t\2\u008c\u008d\7\r\2\2\u008d")
        buf.write("\u008e\7\22\2\2\u008e\u00a4\3\2\2\2\u008f\u0090\7%\2\2")
        buf.write("\u0090\u0091\7\31\2\2\u0091\u0092\5\20\t\2\u0092\u0093")
        buf.write("\7\22\2\2\u0093\u00a4\3\2\2\2\u0094\u0095\7%\2\2\u0095")
        buf.write("\u0096\7\13\2\2\u0096\u0097\5\20\t\2\u0097\u0098\7\f\2")
        buf.write("\2\u0098\u0099\7\31\2\2\u0099\u009a\5\20\t\2\u009a\u009b")
        buf.write("\7\22\2\2\u009b\u00a4\3\2\2\2\u009c\u009d\5\20\t\2\u009d")
        buf.write("\u009e\7\32\2\2\u009e\u009f\7%\2\2\u009f\u00a0\7\31\2")
        buf.write("\2\u00a0\u00a1\5\20\t\2\u00a1\u00a2\7\22\2\2\u00a2\u00a4")
        buf.write("\3\2\2\2\u00a3s\3\2\2\2\u00a3{\3\2\2\2\u00a3\u0083\3\2")
        buf.write("\2\2\u00a3\u0089\3\2\2\2\u00a3\u008f\3\2\2\2\u00a3\u0094")
        buf.write("\3\2\2\2\u00a3\u009c\3\2\2\2\u00a4\17\3\2\2\2\u00a5\u00a6")
        buf.write("\b\t\1\2\u00a6\u00a7\7\33\2\2\u00a7\u00bc\5\20\t\20\u00a8")
        buf.write("\u00bc\7&\2\2\u00a9\u00bc\7!\2\2\u00aa\u00bc\7\"\2\2\u00ab")
        buf.write("\u00bc\7%\2\2\u00ac\u00bc\7#\2\2\u00ad\u00ae\7$\2\2\u00ae")
        buf.write("\u00af\7\20\2\2\u00af\u00b0\7\13\2\2\u00b0\u00b1\5\20")
        buf.write("\t\2\u00b1\u00b2\7\f\2\2\u00b2\u00bc\3\2\2\2\u00b3\u00b4")
        buf.write("\7$\2\2\u00b4\u00b5\7%\2\2\u00b5\u00b6\7\t\2\2\u00b6\u00bc")
        buf.write("\7\r\2\2\u00b7\u00b8\7\t\2\2\u00b8\u00b9\5\20\t\2\u00b9")
        buf.write("\u00ba\7\r\2\2\u00ba\u00bc\3\2\2\2\u00bb\u00a5\3\2\2\2")
        buf.write("\u00bb\u00a8\3\2\2\2\u00bb\u00a9\3\2\2\2\u00bb\u00aa\3")
        buf.write("\2\2\2\u00bb\u00ab\3\2\2\2\u00bb\u00ac\3\2\2\2\u00bb\u00ad")
        buf.write("\3\2\2\2\u00bb\u00b3\3\2\2\2\u00bb\u00b7\3\2\2\2\u00bc")
        buf.write("\u00e1\3\2\2\2\u00bd\u00be\f\16\2\2\u00be\u00bf\7\34\2")
        buf.write("\2\u00bf\u00e0\5\20\t\17\u00c0\u00c1\f\r\2\2\u00c1\u00c2")
        buf.write("\t\2\2\2\u00c2\u00e0\5\20\t\16\u00c3\u00c4\f\f\2\2\u00c4")
        buf.write("\u00c5\7\37\2\2\u00c5\u00e0\5\20\t\r\u00c6\u00c7\f\13")
        buf.write("\2\2\u00c7\u00c8\7 \2\2\u00c8\u00e0\5\20\t\f\u00c9\u00ca")
        buf.write("\f\21\2\2\u00ca\u00cb\7\32\2\2\u00cb\u00d8\7%\2\2\u00cc")
        buf.write("\u00d5\7\t\2\2\u00cd\u00d2\5\20\t\2\u00ce\u00cf\7\23\2")
        buf.write("\2\u00cf\u00d1\5\20\t\2\u00d0\u00ce\3\2\2\2\u00d1\u00d4")
        buf.write("\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3")
        buf.write("\u00d6\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\u00cd\3\2\2\2")
        buf.write("\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d9\7")
        buf.write("\r\2\2\u00d8\u00cc\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00e0")
        buf.write("\3\2\2\2\u00da\u00db\f\17\2\2\u00db\u00dc\7\13\2\2\u00dc")
        buf.write("\u00dd\5\20\t\2\u00dd\u00de\7\f\2\2\u00de\u00e0\3\2\2")
        buf.write("\2\u00df\u00bd\3\2\2\2\u00df\u00c0\3\2\2\2\u00df\u00c3")
        buf.write("\3\2\2\2\u00df\u00c6\3\2\2\2\u00df\u00c9\3\2\2\2\u00df")
        buf.write("\u00da\3\2\2\2\u00e0\u00e3\3\2\2\2\u00e1\u00df\3\2\2\2")
        buf.write("\u00e1\u00e2\3\2\2\2\u00e2\21\3\2\2\2\u00e3\u00e1\3\2")
        buf.write("\2\2\24\26/\65;FN[^ekw\u00a3\u00bb\u00d2\u00d5\u00d8\u00df")
        buf.write("\u00e1")
        return buf.getvalue()


class MiniJavaParser ( Parser ):

    grammarFileName = "MiniJava.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'class'", "'{'", "'public'", "'static'", 
                     "'void'", "'main'", "'('", "'String'", "'['", "']'", 
                     "')'", "'}'", "'extends'", "'int'", "'boolean'", "';'", 
                     "','", "'return'", "'if'", "'else'", "'while'", "'System.out.println'", 
                     "'='", "'.'", "'!'", "'*'", "'+'", "'-'", "'<'", "'&&'", 
                     "'true'", "'false'", "'this'", "'new'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "ID", "I_L", 
                      "WS", "COMMENT" ]

    RULE_goal = 0
    RULE_mainClass = 1
    RULE_classDecl = 2
    RULE_typ = 3
    RULE_varDecl = 4
    RULE_methodDecl = 5
    RULE_statement = 6
    RULE_expression = 7

    ruleNames =  [ "goal", "mainClass", "classDecl", "typ", "varDecl", "methodDecl", 
                   "statement", "expression" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    ID=35
    I_L=36
    WS=37
    COMMENT=38

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class GoalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def mainClass(self):
            return self.getTypedRuleContext(MiniJavaParser.MainClassContext,0)


        def classDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.ClassDeclContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.ClassDeclContext,i)


        def getRuleIndex(self):
            return MiniJavaParser.RULE_goal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGoal" ):
                listener.enterGoal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGoal" ):
                listener.exitGoal(self)




    def goal(self):

        localctx = MiniJavaParser.GoalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_goal)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 16
            self.mainClass()
            self.state = 20
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MiniJavaParser.T__0:
                self.state = 17
                self.classDecl()
                self.state = 22
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MainClassContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(MiniJavaParser.ID)
            else:
                return self.getToken(MiniJavaParser.ID, i)

        def statement(self):
            return self.getTypedRuleContext(MiniJavaParser.StatementContext,0)


        def getRuleIndex(self):
            return MiniJavaParser.RULE_mainClass

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMainClass" ):
                listener.enterMainClass(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMainClass" ):
                listener.exitMainClass(self)




    def mainClass(self):

        localctx = MiniJavaParser.MainClassContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_mainClass)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 23
            self.match(MiniJavaParser.T__0)
            self.state = 24
            self.match(MiniJavaParser.ID)
            self.state = 25
            self.match(MiniJavaParser.T__1)
            self.state = 26
            self.match(MiniJavaParser.T__2)
            self.state = 27
            self.match(MiniJavaParser.T__3)
            self.state = 28
            self.match(MiniJavaParser.T__4)
            self.state = 29
            self.match(MiniJavaParser.T__5)
            self.state = 30
            self.match(MiniJavaParser.T__6)
            self.state = 31
            self.match(MiniJavaParser.T__7)
            self.state = 32
            self.match(MiniJavaParser.T__8)
            self.state = 33
            self.match(MiniJavaParser.T__9)
            self.state = 34
            self.match(MiniJavaParser.ID)
            self.state = 35
            self.match(MiniJavaParser.T__10)
            self.state = 36
            self.match(MiniJavaParser.T__1)
            self.state = 37
            self.statement()
            self.state = 38
            self.match(MiniJavaParser.T__11)
            self.state = 39
            self.match(MiniJavaParser.T__11)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ClassDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(MiniJavaParser.ID)
            else:
                return self.getToken(MiniJavaParser.ID, i)

        def varDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.VarDeclContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.VarDeclContext,i)


        def methodDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.MethodDeclContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.MethodDeclContext,i)


        def getRuleIndex(self):
            return MiniJavaParser.RULE_classDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClassDecl" ):
                listener.enterClassDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClassDecl" ):
                listener.exitClassDecl(self)




    def classDecl(self):

        localctx = MiniJavaParser.ClassDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_classDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 41
            self.match(MiniJavaParser.T__0)
            self.state = 42
            self.match(MiniJavaParser.ID)
            self.state = 45
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniJavaParser.T__12:
                self.state = 43
                self.match(MiniJavaParser.T__12)
                self.state = 44
                self.match(MiniJavaParser.ID)


            self.state = 47
            self.match(MiniJavaParser.T__1)
            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniJavaParser.T__13) | (1 << MiniJavaParser.T__14) | (1 << MiniJavaParser.ID))) != 0):
                self.state = 48
                self.varDecl()
                self.state = 53
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 57
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MiniJavaParser.T__2:
                self.state = 54
                self.methodDecl()
                self.state = 59
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 60
            self.match(MiniJavaParser.T__11)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TypContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MiniJavaParser.ID, 0)

        def getRuleIndex(self):
            return MiniJavaParser.RULE_typ

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyp" ):
                listener.enterTyp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyp" ):
                listener.exitTyp(self)




    def typ(self):

        localctx = MiniJavaParser.TypContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_typ)
        try:
            self.state = 68
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 62
                self.match(MiniJavaParser.T__13)
                self.state = 63
                self.match(MiniJavaParser.T__8)
                self.state = 64
                self.match(MiniJavaParser.T__9)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 65
                self.match(MiniJavaParser.T__14)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 66
                self.match(MiniJavaParser.T__13)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 67
                self.match(MiniJavaParser.ID)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typ(self):
            return self.getTypedRuleContext(MiniJavaParser.TypContext,0)


        def ID(self):
            return self.getToken(MiniJavaParser.ID, 0)

        def getRuleIndex(self):
            return MiniJavaParser.RULE_varDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVarDecl" ):
                listener.enterVarDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVarDecl" ):
                listener.exitVarDecl(self)




    def varDecl(self):

        localctx = MiniJavaParser.VarDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_varDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.typ()
            self.state = 71
            self.match(MiniJavaParser.ID)
            self.state = 72
            self.match(MiniJavaParser.T__15)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MethodDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typ(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.TypContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.TypContext,i)


        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(MiniJavaParser.ID)
            else:
                return self.getToken(MiniJavaParser.ID, i)

        def expression(self):
            return self.getTypedRuleContext(MiniJavaParser.ExpressionContext,0)


        def varDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.VarDeclContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.VarDeclContext,i)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.StatementContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.StatementContext,i)


        def getRuleIndex(self):
            return MiniJavaParser.RULE_methodDecl

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMethodDecl" ):
                listener.enterMethodDecl(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMethodDecl" ):
                listener.exitMethodDecl(self)




    def methodDecl(self):

        localctx = MiniJavaParser.MethodDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_methodDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(MiniJavaParser.T__2)
            self.state = 76
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MiniJavaParser.T__3:
                self.state = 75
                self.match(MiniJavaParser.T__3)


            self.state = 78
            self.typ()
            self.state = 79
            self.match(MiniJavaParser.ID)
            self.state = 80
            self.match(MiniJavaParser.T__6)
            self.state = 92
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniJavaParser.T__13) | (1 << MiniJavaParser.T__14) | (1 << MiniJavaParser.ID))) != 0):
                self.state = 81
                self.typ()
                self.state = 82
                self.match(MiniJavaParser.ID)
                self.state = 89
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MiniJavaParser.T__16:
                    self.state = 83
                    self.match(MiniJavaParser.T__16)
                    self.state = 84
                    self.typ()
                    self.state = 85
                    self.match(MiniJavaParser.ID)
                    self.state = 91
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 94
            self.match(MiniJavaParser.T__10)
            self.state = 95
            self.match(MiniJavaParser.T__1)
            self.state = 99
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 96
                    self.varDecl() 
                self.state = 101
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

            self.state = 105
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniJavaParser.T__1) | (1 << MiniJavaParser.T__6) | (1 << MiniJavaParser.T__18) | (1 << MiniJavaParser.T__20) | (1 << MiniJavaParser.T__21) | (1 << MiniJavaParser.T__24) | (1 << MiniJavaParser.T__30) | (1 << MiniJavaParser.T__31) | (1 << MiniJavaParser.T__32) | (1 << MiniJavaParser.T__33) | (1 << MiniJavaParser.ID) | (1 << MiniJavaParser.I_L))) != 0):
                self.state = 102
                self.statement()
                self.state = 107
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 108
            self.match(MiniJavaParser.T__17)
            self.state = 109
            self.expression(0)
            self.state = 110
            self.match(MiniJavaParser.T__15)
            self.state = 111
            self.match(MiniJavaParser.T__11)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.StatementContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.StatementContext,i)


        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.ExpressionContext,i)


        def ID(self):
            return self.getToken(MiniJavaParser.ID, 0)

        def getRuleIndex(self):
            return MiniJavaParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = MiniJavaParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_statement)
        self._la = 0 # Token type
        try:
            self.state = 161
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 113
                self.match(MiniJavaParser.T__1)
                self.state = 117
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniJavaParser.T__1) | (1 << MiniJavaParser.T__6) | (1 << MiniJavaParser.T__18) | (1 << MiniJavaParser.T__20) | (1 << MiniJavaParser.T__21) | (1 << MiniJavaParser.T__24) | (1 << MiniJavaParser.T__30) | (1 << MiniJavaParser.T__31) | (1 << MiniJavaParser.T__32) | (1 << MiniJavaParser.T__33) | (1 << MiniJavaParser.ID) | (1 << MiniJavaParser.I_L))) != 0):
                    self.state = 114
                    self.statement()
                    self.state = 119
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 120
                self.match(MiniJavaParser.T__11)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 121
                self.match(MiniJavaParser.T__18)
                self.state = 122
                self.match(MiniJavaParser.T__6)
                self.state = 123
                self.expression(0)
                self.state = 124
                self.match(MiniJavaParser.T__10)
                self.state = 125
                self.statement()
                self.state = 126
                self.match(MiniJavaParser.T__19)
                self.state = 127
                self.statement()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 129
                self.match(MiniJavaParser.T__20)
                self.state = 130
                self.match(MiniJavaParser.T__6)
                self.state = 131
                self.expression(0)
                self.state = 132
                self.match(MiniJavaParser.T__10)
                self.state = 133
                self.statement()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 135
                self.match(MiniJavaParser.T__21)
                self.state = 136
                self.match(MiniJavaParser.T__6)
                self.state = 137
                self.expression(0)
                self.state = 138
                self.match(MiniJavaParser.T__10)
                self.state = 139
                self.match(MiniJavaParser.T__15)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 141
                self.match(MiniJavaParser.ID)
                self.state = 142
                self.match(MiniJavaParser.T__22)
                self.state = 143
                self.expression(0)
                self.state = 144
                self.match(MiniJavaParser.T__15)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 146
                self.match(MiniJavaParser.ID)
                self.state = 147
                self.match(MiniJavaParser.T__8)
                self.state = 148
                self.expression(0)
                self.state = 149
                self.match(MiniJavaParser.T__9)
                self.state = 150
                self.match(MiniJavaParser.T__22)
                self.state = 151
                self.expression(0)
                self.state = 152
                self.match(MiniJavaParser.T__15)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 154
                self.expression(0)
                self.state = 155
                self.match(MiniJavaParser.T__23)
                self.state = 156
                self.match(MiniJavaParser.ID)
                self.state = 157
                self.match(MiniJavaParser.T__22)
                self.state = 158
                self.expression(0)
                self.state = 159
                self.match(MiniJavaParser.T__15)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MiniJavaParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(MiniJavaParser.ExpressionContext,i)


        def I_L(self):
            return self.getToken(MiniJavaParser.I_L, 0)

        def ID(self):
            return self.getToken(MiniJavaParser.ID, 0)

        def getRuleIndex(self):
            return MiniJavaParser.RULE_expression

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpression" ):
                listener.enterExpression(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpression" ):
                listener.exitExpression(self)



    def expression(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MiniJavaParser.ExpressionContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 14
        self.enterRecursionRule(localctx, 14, self.RULE_expression, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 185
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.state = 164
                self.match(MiniJavaParser.T__24)
                self.state = 165
                self.expression(14)
                pass

            elif la_ == 2:
                self.state = 166
                self.match(MiniJavaParser.I_L)
                pass

            elif la_ == 3:
                self.state = 167
                self.match(MiniJavaParser.T__30)
                pass

            elif la_ == 4:
                self.state = 168
                self.match(MiniJavaParser.T__31)
                pass

            elif la_ == 5:
                self.state = 169
                self.match(MiniJavaParser.ID)
                pass

            elif la_ == 6:
                self.state = 170
                self.match(MiniJavaParser.T__32)
                pass

            elif la_ == 7:
                self.state = 171
                self.match(MiniJavaParser.T__33)
                self.state = 172
                self.match(MiniJavaParser.T__13)
                self.state = 173
                self.match(MiniJavaParser.T__8)
                self.state = 174
                self.expression(0)
                self.state = 175
                self.match(MiniJavaParser.T__9)
                pass

            elif la_ == 8:
                self.state = 177
                self.match(MiniJavaParser.T__33)
                self.state = 178
                self.match(MiniJavaParser.ID)
                self.state = 179
                self.match(MiniJavaParser.T__6)
                self.state = 180
                self.match(MiniJavaParser.T__10)
                pass

            elif la_ == 9:
                self.state = 181
                self.match(MiniJavaParser.T__6)
                self.state = 182
                self.expression(0)
                self.state = 183
                self.match(MiniJavaParser.T__10)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 223
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,17,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 221
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
                    if la_ == 1:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 187
                        if not self.precpred(self._ctx, 12):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 12)")

                        self.state = 188
                        self.match(MiniJavaParser.T__25)
                        self.state = 189
                        self.expression(13)
                        pass

                    elif la_ == 2:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 190
                        if not self.precpred(self._ctx, 11):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 11)")
                        self.state = 191
                        _la = self._input.LA(1)
                        if not(_la==MiniJavaParser.T__26 or _la==MiniJavaParser.T__27):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 192
                        self.expression(12)
                        pass

                    elif la_ == 3:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 193
                        if not self.precpred(self._ctx, 10):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 10)")

                        self.state = 194
                        self.match(MiniJavaParser.T__28)
                        self.state = 195
                        self.expression(11)
                        pass

                    elif la_ == 4:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 196
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")

                        self.state = 197
                        self.match(MiniJavaParser.T__29)
                        self.state = 198
                        self.expression(10)
                        pass

                    elif la_ == 5:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 199
                        if not self.precpred(self._ctx, 15):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 15)")
                        self.state = 200
                        self.match(MiniJavaParser.T__23)
                        self.state = 201
                        self.match(MiniJavaParser.ID)
                        self.state = 214
                        self._errHandler.sync(self)
                        la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                        if la_ == 1:
                            self.state = 202
                            self.match(MiniJavaParser.T__6)
                            self.state = 211
                            self._errHandler.sync(self)
                            _la = self._input.LA(1)
                            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MiniJavaParser.T__6) | (1 << MiniJavaParser.T__24) | (1 << MiniJavaParser.T__30) | (1 << MiniJavaParser.T__31) | (1 << MiniJavaParser.T__32) | (1 << MiniJavaParser.T__33) | (1 << MiniJavaParser.ID) | (1 << MiniJavaParser.I_L))) != 0):
                                self.state = 203
                                self.expression(0)
                                self.state = 208
                                self._errHandler.sync(self)
                                _la = self._input.LA(1)
                                while _la==MiniJavaParser.T__16:
                                    self.state = 204
                                    self.match(MiniJavaParser.T__16)
                                    self.state = 205
                                    self.expression(0)
                                    self.state = 210
                                    self._errHandler.sync(self)
                                    _la = self._input.LA(1)



                            self.state = 213
                            self.match(MiniJavaParser.T__10)


                        pass

                    elif la_ == 6:
                        localctx = MiniJavaParser.ExpressionContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expression)
                        self.state = 216
                        if not self.precpred(self._ctx, 13):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 13)")
                        self.state = 217
                        self.match(MiniJavaParser.T__8)
                        self.state = 218
                        self.expression(0)
                        self.state = 219
                        self.match(MiniJavaParser.T__9)
                        pass

             
                self.state = 225
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,17,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[7] = self.expression_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression_sempred(self, localctx:ExpressionContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 12)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 11)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 10)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 15)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 13)
         




