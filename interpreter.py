stack = []

def getword(s):
    word = ''
    i = 0
    while s[i] != ' ' and s[i] != '\n':
        word += s[i]
        i += 1
    return word

def foo (s, name):
    global stack
    vars = []
    start = 0
    for i in range(len(s)):
        if s[i] == ('Method ' + name + '\n'):
            i += 1
            if s[i][0] == '#':
                for j in s[i]:
                    if j == '#':
                        vars.append(0)
                i += 1
            start = i
            while True:
                word = getword(s[i])
                if word == 'iload':
                    val = getword(s[i][len(word) + 1:])
                    stack.append(vars[int(val[1:])])
                elif word == 'iconst':
                    val = getword(s[i][len(word) + 1:])
                    stack.append(val)
                elif word == 'istore':
                    val = getword(s[i][len(word) + 1:])
                    el = int(stack.pop())
                    vars[int(val[1:])] = el
                elif word == 'iadd':
                    fir = int(stack.pop())
                    sec = int(stack.pop())
                    stack.append(fir + sec)
                elif word == 'imul':
                    fir = int(stack.pop())
                    sec = int(stack.pop())
                    stack.append(fir * sec)
                elif word == 'isub':
                    sec = int(stack.pop())
                    fir = int(stack.pop())
                    stack.append(fir - sec)
                elif word == 'idiv':
                    sec = int(stack.pop())
                    fir = int(stack.pop())
                    stack.append(fir / sec)
                elif word == 'ilt':
                    sec = int(stack.pop())
                    fir = int(stack.pop())
                    if fir < sec:
                        stack.append(1)
                    else:
                        stack.append(0)
                elif word == 'ieq':
                    fir = int(stack.pop())
                    sec = int(stack.pop())
                    if fir == sec:
                        stack.append(1)
                    else:
                        stack.append(0)
                elif word == 'iand':
                    fir = int(stack.pop())
                    sec = int(stack.pop())
                    if fir * sec != 0:
                        stack.append(1)
                    else:
                        stack.append(0)
                elif word == 'ior':
                    fir = int(stack.pop())
                    sec = int(stack.pop())
                    if fir + sec != 0:
                        stack.append(1)
                    else:
                        stack.append(0)
                elif word == 'inot':
                    el = int(stack.pop())
                    if el == 0:
                        stack.append(1)
                    else:
                        stack.append(0)
                elif word == 'goto':
                    val = getword(s[i][len(word) + 1:])
                    i = start + int(val) - 1
                elif word == 'if_false':
                    val = getword(s[i][len(word) + 1:])
                    el = int(stack.pop())
                    if el == 0:
                        i = start + int(val) - 1
                elif word == 'invokevirtual':
                    val = getword(s[i][len(word) + 1:])
                    foo(s, val)
                elif word == 'print':
                    el = int(stack.pop())
                    print(el)
                elif word == 'return' or word == 'stop':
                    return
                i += 1


fin = open('class.tjc')
lines = []
s = fin.readline()
while s != '':
    lines.append(s)
    s = fin.readline()
foo(lines, 'main')

