#coding cp1251
from antlr4 import *
from MiniJavaLexer import MiniJavaLexer
from MiniJavaParser import MiniJavaParser


goalcontext = "<class 'MiniJavaParser.MiniJavaParser.GoalContext'>"
mainclass = "<class 'MiniJavaParser.MiniJavaParser.MainClassContext'>"
terminal = "<class 'antlr4.tree.Tree.TerminalNodeImpl'>"
statement = "<class 'MiniJavaParser.MiniJavaParser.StatementContext'>"
expression = "<class 'MiniJavaParser.MiniJavaParser.ExpressionContext'>"
classdecl = "<class 'MiniJavaParser.MiniJavaParser.ClassDeclContext'>"
method = "<class 'MiniJavaParser.MiniJavaParser.MethodDeclContext'>"
typ = "<class 'MiniJavaParser.MiniJavaParser.TypContext'>"
var = "<class 'MiniJavaParser.MiniJavaParser.VarDeclContext'>"


type_of_error = ''
like_static = False
class Scope:
    def __init__(self, prev, type, name):
        self.parent = prev
        self.children = []
        self.ids = []
        self.identyfied = []
        self.name = name
        self.type = type
        self.params = []
        self.pos = 0
        self.clas = ''
        self.funk = False
        self.extend = None
        self.static = False
        self.returnthis = False

    # геттеры/сеттеры
    def get_parent(self):
        return self.parent

    def set_parent(self, par):
        self.parent = par

    def get_children(self):
        return self.children

    def get_child(self, a):
        return self.children[a]

    def add_child(self, chil):
        self.children.append(chil)

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_type(self):
        return self.type

    def set_type(self, type):
        self.type = type

    def get_pos(self):
        return self.pos

    def set_pos(self, pos):
        self.pos = pos

    def get_ids(self):
        return self.ids

    def add_id(self, a):
        self.ids.append(a)

    def get_id(self, a):
        return self.ids[a]

    def get_param(self):
        return self.params

    def add_param(self, el):
        self.params.append(el)

    def get_clas(self):
        return self.clas

    def set_clas(self, clas):
        self.clas = clas

    def get_funk(self):
        return self.funk

    def set_funk(self, funk):
        self.funk = funk
    #

    def next_child(self):
        nextChild = None
        pos = self.get_pos()
        if (pos >= len(self.get_children())):
            self.get_children().append(nextChild)
        else:
            nextChild = self.get_child(pos)
        pos += 1
        self.set_pos(pos)
        return nextChild

    def lookup_class(self, name):
        for j in self.children:
            if j.get_name() == name and j.get_funk() == False:
                return j
        if self.get_parent():
            return self.get_parent().lookup_class(name)
        return None

    def lookup_method(self, name):
        p = None
        for j in self.children:
            if j.get_name() == name and j.get_funk() == True:
                return j
        t = False
        for j in self.children:
            if j.get_funk() == False:
                for k in j.children:
                    if k.get_name() == name and k.get_funk() == True:
                        return k
        if self.get_parent():
            return self.get_parent().lookup_method(name)
        return None

    def lookup_var(self, name):
        for i in self.ids:
            if i[1] == name:
                return i
        if self.get_parent():
            return self.get_parent().lookup_var(name)
        return None

    def uniq(self, id):
        for i in self.children:
            if i.name == id[1] and i.type == id[0]:
                return False
        return True

    def go_null(self):
        self.pos = 0
        for i in self.children:
            i.go_null()

    def print(self, tab):
        if self.extend:
            print('\t' * tab + self.type + ' ' + self.name + ' extends ' + self.extend)
        else:
            print('\t' * tab + self.type + ' ' + self.name)
        for i in self.children:
            i.print(tab + 1)
        for i in self.ids:
            print('\t' * (tab + 1) + i[0] + ' ' + i[1])

isfoo = False

class SymbolTable:
    def __init__(self, root):
        self.root = root
        self.current = root
        self.expressions = []

    # геттеры/сеттеры
    def get_root(self):
        return self.root

    def set_root(self, root):
        self.root = root

    def get_current(self):
        return self.current

    def set_current(self, cur):
        self.current = cur

    def get_expression(self):
        return self.expressions

    def add_expression(self, a):
        self.expressions.append(a)
    #

    def enter_scope(self):
        self.set_current(self.current.next_child())

    def exit_scope(self):
        if self.current:
            self.set_current(self.current.get_parent())

    def put(self, type, name):
        self.get_current().add_id((type, name))

    def lookup_class_var(self, clas, var):
        if clas == 'this':
            clas = self.get_current().get_clas()
        try:
            idd = self.get_current().lookup_var(clas)[0]
        except:
            idd = clas
        cl = self.get_current().lookup_class(idd)
        if not cl:
            return None
        children = cl.get_ids()
        for i in children:
            if i[1] == var:
                return i
        if cl.extend:
            classs = self.get_current().lookup_clas(cl.extend)
            children = classs.get_ids()
            for i in children:
                if i[1] == var:
                    return i
        return None

    def lookup_class_method(self, clas, method):
        if clas == 'this':
            clas = self.get_current().get_clas()
        try:
            idd = self.get_current().lookup_var(clas)[0]
        except:
            idd = clas
        cl = self.get_current().lookup_class(idd)
        if not cl:
            return None
        children = cl.get_children()
        for i in children:
            if i.name == method and i.funk == True:
                return i
        if cl.extend:
            classs = self.get_current().lookup_clas(cl.extend)
            children = classs.get_children()
            for i in children:
                if i.name == method and i.funk == True:
                    return i
        return None

    def lookup_var(self, name):
        return self.get_current().lookup_var(name)

    def lookup_class(self, name):
        return self.get_current().lookup_class(name)

    def checking_expr(self, expr):
        global type_of_error
        global isfoo
        global like_static
        symb = expr.get_symb()
        if symb == '[':
            if expr.get_el() == 'int':
                ppp = self.checking_expr(expr.get_children()[0])
                if ppp == 'int':
                    return 'int[]'
                else:
                    if ppp != None:
                        type_of_error = 'ошибка выражения типа new int[a], ожидается a = int, встречен a = ' + ppp
                    return None
            else:
                pp1 = self.checking_expr(expr.get_children()[0])
                pp2 = self.checking_expr(expr.get_children()[1])
                if pp1 == 'int[]' and  pp2 == 'int':
                    return 'int'
                type_of_error = 'ошибка в конструкции a[b], ожидается a = int[], b = int, найдены a = ' + pp1 + ' b = ' + pp2
                return None
        if symb == 'new':
            return expr.get_el()
        if symb == '.':
            isfoo = True
            a = expr.get_children()
            if expr.get_el() == 'length':
                va = self.checking_expr(a[0])
                if va == 'int[]':
                    return 'int'
                else:
                    if va != None:
                        type_of_error = 'ошибка в конструкции a.length, ожидается a = int[] найдено a = ' + va
                    return None
            b = self.checking_expr(a[0])
            if b == None:
                return None
            meth = self.lookup_class_method(b, expr.get_el())
            if meth == None:
                stat = self.get_current().static
                meth = self.lookup_class_var(b, expr.get_el())
                if meth and stat:
                    type_of_error = ('попытка в объявлении статичного метода "' + str(self.get_current().get_clas()) + '.' + str(self.get_current().get_name()) + '" вызвать нестатичное поле класса "' + str(b) + '.' + str(expr.get_el()) + '"')
                    return None
                if meth and like_static:
                    type_of_error = (
                    'попытка вызвать не статичное поле "' + str(b) + '.' + str(
                        expr.get_el()) + '" как статичное' )
                    return None
                if meth == None:
                    type_of_error = ('вызываемая конструкция типа "' + str(b) + '.' + str(expr.get_el()) + '" не найдена')
                    return None
                return meth[0]
            if like_static and meth.static == False:
                type_of_error = ('попытка вызова нестатичного метода "' + str(b) + '.' + str(expr.get_el()) + '" как статичного')
                return None
            like_static = False
            changed = False
            return meth.get_type()
        if symb:
            a = self.checking_expr(expr.get_children()[0])
            b = self.checking_expr(expr.get_children()[1])
            if a == None or b == None:
                return None
            if symb == '+' or symb == '-' or symb == '*':
                if a == 'int' and b == 'int':
                    return 'int'
                type_of_error = 'ошибка в выражении a ' + symb + ' b, ожидается a = int, b = int найдено a = ' + a+ ' b = ' + b
                return None
            elif symb == '&':
                if a == 'boolean' and b == 'boolean':
                    return 'boolean'
                else:
                    type_of_error = 'ошибка в выражении a ' + symb + ' b, ожидается a = boolean, b = boolean найдено a = ' + a + ' b = ' + b
                    return None
            elif symb == '<':
                if a == 'int' and b == 'int':
                    return 'boolean'
                else:
                    type_of_error = 'ошибка в выражении a ' + symb + ' b, ожидается a = int, b = int найдено a = ' + a + ' b = ' + b
                    return None
            elif symb == '[':
                if a == 'int[]' and b == 'int':
                    return 'int'
                type_of_error = 'ошибка в выражении a ' + symb + ' b, ожидается a = int[], b = int найдено a = ' + a + ' b = ' + b
                return None
        else:
            a = expr.get_children()
            if len(a) == 1:
                va = self.checking_expr(expr.get_children()[0])
                if expr.get_el() == 'length':
                    if va == 'int[]':
                        return 'int'
                    else:
                        type_of_error = 'ошибка в выражении a.length, ожидается a = int[] найдено a = ' + a
                        return None
                return va
            else:
                el = expr.get_el()
                if el[0] in ['0','1','2','3','4','5','6','7','8','9']:
                    return 'int'
                elif el == 'true' or el == 'false':
                    return 'boolean'
                elif el == 'this':
                    return self.get_current().get_clas()
                else:
                    try:
                        typ = self.get_current().lookup_var(el)
                    except:
                        return expr.get_el()
                    if typ == None:
                        typ = self.get_current().lookup_class(el)
                        if typ:
                            like_static = True
                            return el
                        else:
                            type_of_error = 'ошибка, попытка обратиться к незнакомому объекту "' + el + '"'
                            return None
                    else:
                        clas = self.get_current().get_clas()
                        clas = self.lookup_class(clas)
                        counter = 0
                        for i in clas.get_ids():
                            if i[1] == el:
                                if self.get_current().static:
                                    type_of_error = ('попытка в объявлении статичного метода "' + str(
                                        self.get_current().get_clas()) + '.' + str(
                                        self.get_current().get_name()) + '" вызвать нестатичное поле класса "' + str(
                                        self.get_current().get_clas()) + '.' + str(expr.get_el()) + '"')
                                    return None
                    if typ:
                        return typ[0]
                    else:
                        return None

    def go_null(self):
        self.current = self.root
        self.root.go_null()

    def print(self):
        self.root.print(0)


class Expr:
    def __init__(self, root):
        self.root = root
        self.children = []
        self.symb = None
        self.el = None

    def add_child(self, a):
        self.children.append(a)

    def get_children(self):
        return self.children

    def get_last_child(self):
        return self.children[len(self.children) - 1]

    def set_root(self, a):
        self.root = a

    def get_root(self):
        if self.root:
            return self.root
        return self

    def set_symb(self, a):
        self.symb = a

    def get_symb(self):
        return self.symb

    def get_el(self):
        return self.el

    def set_el(self, a):
        self.el = a


def build_expr(s, pos):
    expr = Expr(None)
    num = 0
    while num >= 0:
        if s[pos] == '(' and s[pos + 1] != ' ':
            num += 1
            expr.add_child(Expr(expr))
            expr = expr.get_last_child()
        elif s[pos] == ')' and s[pos - 1] != ' ':
            num -= 1
            expr = expr.get_root()
        elif s[pos] == '+' or s[pos] == '-' or s[pos] == '*' or s[pos] == '<' or s[pos] == '&' or s[pos] == '[' or s[pos] == '.':
            expr.set_symb(s[pos])
        elif not (s[pos] in ['(',')',';','}','{',']',' ',',']) and (s[pos - 1] == ' ' or s[pos - 1] == '('):
            word , pos = get_word(s, pos)
            pos -= 1
            while word[len(word) - 1] == ')':
                word = word[:len(word) - 1]
                pos -= 1
            if word == 'new':
                expr.set_symb(word)
            else:
                expr.set_el(word)
        pos += 1
    return expr, pos


def get_word(s, i):
    start = i
    while s[i] != ' ':
        i += 1
    return s[start:i], i


def BuildSymbolTable(tree, parser):
    global isfoo
    s = tree.toStringTree(tree, parser)
    clas = 'prog'
    table = SymbolTable(Scope(None, 'prog', 'program'))
    i = 1
    checked_expr = 0
    exist = True
    type_is_correct = True
    word = ''
    scobki = [True]
    element = tree
    view = [0]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                while str(type(element.children[view[len(view) - 1]])) not in [mainclass, classdecl]:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                clas = word
                if table.lookup_class(word) != None:
                    print('попытка добавить класс дублирующий название уже созданного класса "' + word + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    exit(0)
                table.get_current().add_child(Scope(table.get_current(),'class', word))
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_clas(clas)
                table.enter_scope()
                word, i = get_word(s, i + 1)
                if word == 'extends':
                    word, i = get_word(s, i + 1)
                    table.get_current().extend = word
                scobki.append(True)
            elif word == 'methodDecl':
                while str(type(element.children[view[len(view) - 1]])) != method:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                is_static = False
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                if word1 == 'tatic':
                    word1, i = get_word(s, i + 1)
                    is_static = True
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if table.lookup_class_method(table.get_current().get_clas(), word2) != None:
                    print('попытка перегрузить метод "' + table.get_current().get_clas() + '.' + word2 + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    exit(0)
                table.get_current().add_child(Scope(table.get_current(), word1, word2))
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_clas(clas)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.get_current().get_child(len(table.get_current().get_children()) - 1).set_funk(True)
                table.enter_scope()
                table.get_current().static = is_static
                name = word2
                numch = 0
                while word == '(typ':
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    if table.lookup_var(word2):
                        print('попытка добавить параметр в функцию "' + table.get_current().get_clas() + '.' + name + '" дублирующий добавленный ранее "' + word2 + '", строка ' + str(element.children[numch].start.line) + ', столбец ' + str(element.children[numch].start.column))
                        exit(0)
                    table.get_current().add_param((word, word2))
                    table.put(word, word2)
                    table.get_current().identyfied.append(True)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                    numch += 1
                scobki.append(True)
            elif word == 'varDecl':
                while str(type(element.children[view[len(view) - 1]])) != var:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                    if table.lookup_var(word2):
                        print('попытка создать дублирующуюся переменную "' + word2 + '" в "' + table.get_current().get_name() + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                    table.put(word1 + '[]', word2)
                    table.get_current().identyfied.append(False)
                else:
                    if table.lookup_var(word2):
                        print('попытка создать дублирующуюся переменную "' + word2 + '" в "' + table.get_current().get_name() + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                    table.put(word1, word2)
                    table.get_current().identyfied.append(False)
                scobki.append(False)
            elif word == 'statement':
                while str(type(element.children[view[len(view) - 1]])) != statement:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
            elif word == 'expression':
                while str(type(element.children[view[len(view) - 1]])) != expression:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1 == 'true' or word1[:5] == 'true)' or word1 == 'false' or word1[:6] == 'false)' or word1 == 'this'or  word1[:5] == 'this)' or word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            element = element.parentCtx
            view.pop()
            if val:
                try:
                    table.exit_scope()
                except:
                    return table
            i += 1
        elif s[i] == '.':
            word, i = get_word(s, i + 2)
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    i -= 1
                continue
            else:
                j = i - len(word) - 5
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    i -= 1
                while s[j] != ' ':
                    j -= 1
                word1, j = get_word(s, j + 1)
                word1 = word1[:len(word1) - 1]
        else:
            i += 1
    return table


expression_with_probeli = []


def check_exist(tree, parser, table):
    global isfoo
    global expression_with_probeli
    global type_of_error
    global like_static
    probeli = 5
    s = tree.toStringTree(tree, parser)
    clas = 'prog'
    i = 1
    checked_expr = 0
    exist = True
    type_is_correct = True
    word = ''
    scobki = [True]
    element = tree
    view = [0]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                while str(type(element.children[view[len(view) - 1]])) not in [mainclass, classdecl]:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                probeli += len(word) + 1
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                clas = word
                table.enter_scope()
                scobki.append(True)
            elif word == 'methodDecl':
                while str(type(element.children[view[len(view) - 1]])) != method:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                probeli += len(word) + 1
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                if word1 == 'tatic':
                    word1, i = get_word(s, i + 1)
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.enter_scope()
                while word == '(typ':
                    probeli += len(word) + 1
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                scobki.append(True)
            elif word == 'varDecl':
                while str(type(element.children[view[len(view) - 1]])) != var:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                probeli += len(word) + 2
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word1 not in ('int', 'boolean'):
                    val = table.lookup_class(word1)
                    if val == None:
                        print('создание переменной несуществующего типа ' + word1 + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                scobki.append(False)
            elif word == 'statement':
                while str(type(element.children[view[len(view) - 1]])) != statement:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                probeli += len(word) + 1
                word1, i = get_word(s, i + 1)
                if not(word1 == '{' or word1 == 'if' or word1 == 'while' or word1 == 'System.out.println' or word1 == '(expression'):
                    res = table.lookup_var(word1)
                    if not res:
                        exist = False
                        print('вызываемый объект "' + word1 + '" не найден, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                if word1 == '(expression':
                    i -= 15
                scobki.append(False)
            elif word == 'expression':
                while str(type(element.children[view[len(view) - 1]])) != expression:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                probeli += len(word) + 1
                expr, checked_expr = build_expr(s, i - len(word) + 1)
                foo = False
                if s[i + 1] == '(' and s[i + 2] == ' ':
                    foo = True
                jj = checked_expr
                isneed = False
                word3, l = get_word(s, jj + 3)
                if s[jj + 1] == '.':
                    isneed = True
                    while s[jj] != '=':
                        if s[jj] == ')' and s[jj - 1] != ' ':
                            isneed = False
                            break
                        jj += 1
                prevex = None
                like_static = False
                if isneed:
                    prevex = table.checking_expr(expr)
                    exp = Expr(None)
                    exp.symb = '.'
                    exp.el = word3
                    exp.children.append(expr)
                    expr.root = exp
                    ex = table.checking_expr(exp)
                else:
                    ex = table.checking_expr(expr)
                if isfoo:
                    checked_expr = i
                    while s[checked_expr] != '.':
                        checked_expr += 1
                isfoo = False
                table.add_expression((i - 11, ex))
                if prevex:
                    table.add_expression((i - 11, prevex))
                if not ex:
                    type_is_correct = False
                    print(type_of_error + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    if type_of_error != '':
                        exit(0)
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1 == 'true' or word1[:5] == 'true)' or word1 == 'false' or word1[:6] == 'false)' or word1 == 'this'or  word1[:5] == 'this)' or word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                    if word1 == 'int':
                        continue
                    else:
                        res = table.lookup_class(word1)
                        if not res:
                            exist = False
                            print('вызываемый объект "' + word1 + '" не найден, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                            continue
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    res = table.lookup_class(word1)
                    if res == None:
                        res = table.lookup_var(word1)
                    if not res:
                        print('вызываемый объект "' + word1 + '" не найден, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exist = False
                        exit(0)
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            element = element.parentCtx
            view.pop()
            probeli += 1
            if val:
                try:
                    table.exit_scope()
                except:
                    return exist, type_is_correct
            i += 1
        elif s[i] == '.':
            startexpr = 0
            dot = i
            word, i = get_word(s, i + 2)
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    i -= 1
                continue
            else:
                j = dot - 3
                scob = 1
                while scob > 0:
                    if s[j] == ')' and s[j - 1] != ' ':
                        scob += 1
                    if s[j] == '(' and s[j + 1] != ' ':
                        scob -= 1
                    j -= 1
                startexpr = j
                while s[j] != '.':
                    j += 1
                changed = False
                if j != dot:
                    word2, j = get_word(s, j + 2)
                    j = j - len(word2) - 5
                    while word[len(word) - 1] == ')':
                        word = word[:len(word) - 1]
                        i -= 1
                        changed = True
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                    res = table.lookup_class_method(word1, word2)
                    word1 = res.get_type()
                else:
                    j = i - len(word) - 5
                    while word[len(word) - 1] == ')':
                        word = word[:len(word) - 1]
                        i -= 1
                        changed = True
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    changed = True
                for ex in table.get_expression():
                    if ex[0] == startexpr + 13:
                        word1 = ex[1]
                if changed or s[i + 1] == '=':
                    res = table.lookup_class_var(word1, word)
                else:
                    res = table.lookup_class_method(word1, word)
                if not res:
                    print('вызываемый объект "' + word1 + '.' + word + '" не найден, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    exit(0)
                    exist = False
                    continue
        else:
            i += 1
    return exist, type_is_correct


# отображаем дерево в консоли
def disp_tree(tree, parser):
    s = tree.toStringTree(tree, parser)
    i = 0
    p = 1
    for i in range(len(s)):
        if i == len(s) - 1:
            if s[i] == '(' and s[i + 1] != ' ':
                print('<')
                print('\t' * p, end='')
            elif s[i] == ')' and s[i - 1] != ' ':
                print('>')
                print('\t' * (p), end='')
            else:
                print(s[i], end='')
        else:
            if s[i + 1] == '(' and s[i + 2] != ' ':
                print(s[i])
                print('\t' * p, end='')
                p += 1
            elif s[i + 1] == ')' and s[i] != ' ':
                p -= 1
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i])
                    print('\t' * p, end='')
            else:
                if s[i] == '(' and s[i + 1] != ' ':
                    print('<')
                    print('\t' * p, end='')
                elif s[i] == ')' and s[i - 1] != ' ':
                    print('>')
                    print('\t' * (p), end='')
                else:
                    print(s[i], end='')

# проходим по дереву, ищем ошибки, не нашли - значит строка синтаксически корректна
def iscorrect(tree):
    try:
        if (str(type(tree.exception)) != "<class 'NoneType'>"):
            return False
        for i in tree.children:
            if str(type(i)) == "<class 'antlr4.tree.Tree.ErrorNodeImpl'>":
                return False
            vol = iscorrect(i)
            if not vol:
                return False
        return True
    except:
        return True


def checking_stat(tree, parser, table):
    s = tree.toStringTree(tree, parser)
    i = 0
    arr = table.get_expression()
    stat = True
    funk = True
    word = ''
    scobki = [True]
    element = tree
    view = [0]
    while i < len(s):
        if s[i] == '(' and s[i + 1] != ' ':
            word, i = get_word(s, i + 1)
            if word == 'mainClass' or word == 'classDecl':
                while str(type(element.children[view[len(view) - 1]])) not in [classdecl, mainclass]:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                table.enter_scope()
                scobki.append(True)
            elif word == 'methodDecl':
                while str(type(element.children[view[len(view) - 1]])) != method:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                name = ''
                word1, i = get_word(s, i + 1)
                i += 1
                word1, i = get_word(s, i + 1)
                if word1 == 'tatic':
                    word1, i = get_word(s, i + 1)
                word1, i = get_word(s, i + 1)
                word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                name = word2
                word, i = get_word(s, i + 1)
                word, i = get_word(s, i + 1)
                while word == '(typ':
                    word, i = get_word(s, i + 1)
                    word = word[:len(word) - 1]
                    word2, i = get_word(s, i + 1)
                    if s[i + 1] == ',':
                        word, i = get_word(s, i + 1)
                        word, i = get_word(s, i + 1)
                table.enter_scope()
                j = i
                word, j = get_word(s, j + 1)
                while word != 'return':
                    word, j = get_word(s, j + 1)
                l = j
                word, l = get_word(s, l + 1)
                word, l = get_word(s, l + 1)
                if word == 'this)':
                    table.get_current().returnthis = True
                for k in table.get_expression():
                    if k[0] == j + 1 and k[1] != word1:
                        funk = False
                        print('ошибка, попытка вернуть из функции "' + name + '" типа ' + word1 + ' значение типа ' + k[1] + ', строка ' + str(element.children[len(element.children) - 3].start.line) + ', столбец ' + str(element.children[len(element.children) - 3].start.column))
                        exit(0)
                scobki.append(True)
            elif word == 'varDecl':
                while str(type(element.children[view[len(view) - 1]])) != var:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word1, i = get_word(s, i + 2)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                word2, i = get_word(s, i + 1)
                if word2 == '[':
                    word2, i = get_word(s, i + 1)
                    word2, i = get_word(s, i + 1)
                scobki.append(False)
            elif word == 'statement':
                while str(type(element.children[view[len(view) - 1]])) != statement:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                scobki.append(False)
                word1, i = get_word(s, i + 1)
                while word1[len(word1) - 1] is ')':
                    word1 = word1[:len(word1) - 1]
                    i -= 1
                if word1 == 'if' or word1 == 'while':
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'boolean'):
                            stat = False
                            print('ошибка в конструкции ' + word1 + ' (a), ожидается a = boolean, найдено a = ' + j[1] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                elif word1 == 'System.out.println':
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'int'):
                            stat = False
                            print('ошибка в конструкции ' + word1 + ' (a), ожидается a = int, найдено a = ' + j[1] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                elif word1 == '{':
                    continue
                elif s[i + 1] == '=':
                    typ = table.lookup_var(word1)
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != typ[0]):
                            stat = False
                            print('ошибка в присвоении значения объекту "' + word1 + '" ожидаются одинаковые типы, найдено ' + j[1] + ' = ' + typ[0] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                elif s[i + 1] == '[':
                    typ = table.lookup_var(word1)
                    if typ[0] != 'int[]':
                        stat = False
                        print('ошибка, попытка получить элемент массива от объекта "' + word1 + '", который имеет тип ' + typ[0] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                    for j in arr:
                        if (j[0] == i + 3 and j[1] != 'int'):
                            stat = False
                            print('ошибка, попытка вызвать элемент массива "' + word1 + '", с порядковым номером типа ' + j[1] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                    while s[i] != '=':
                        i += 1
                    while not (str(type(element.children[view[len(view) - 1]])) == terminal and element.children[view[len(view) - 1]].symbol.text == '='):
                        view[len(view) - 1] += 1
                    for j in arr:
                        if (j[0] == i + 2 and j[1] != 'int'):
                            stat = False
                            print('ошибка, попытка присвоить элементу целочисленного массива "' + word1 + '" значение типа ' + j[1] + ', строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                if word1 == '(expression':
                    i -= 15
            elif word == 'expression':
                while str(type(element.children[view[len(view) - 1]])) != expression:
                    view[len(view) - 1] += 1
                element = element.children[view[len(view) - 1]]
                view[len(view) - 1] += 1
                view.append(0)
                word1, i = get_word(s, i + 1)
                scobki.append(False)
                if word1 == '(expression':
                    i -= 15
                elif word1[:4] == 'true' or word1[:5] == 'true)' or word1[:5] == 'false' or word1[
                                                                                            :6] == 'false)' or word1[
                                                                                                               :4] == 'this' or word1[
                                                                                                                                :5] == 'this)' or \
                                word1[0] == '!' or word1 == '(':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1[0] == '0' or word1[0] == '1' or word1[0] == '2' or word1[0] == '3' or word1[0] == '4' or \
                                word1[0] == '5' or word1[0] == '6' or word1[0] == '7' or word1[0] == '8' or word1[
                    0] == '9':
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
                    continue
                elif word1 == 'new':
                    word1, i = get_word(s, i + 1)
                    if word1 == 'int':
                        continue
                    else:
                        res = table.lookup_class(word1)
                        if not res:
                            print('вызываемый объект ' + word1 + ' не найден, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            exit(0)
                            exist = False
                            continue
                else:
                    while word1[len(word1) - 1] is ')':
                        word1 = word1[:len(word1) - 1]
                        i -= 1
        elif s[i] == ')' and s[i - 1] != ' ':
            val = scobki.pop(len(scobki) - 1)
            element = element.parentCtx
            view.pop()
            if val:
                try:
                    table.exit_scope()
                except:
                    return stat, funk
            i += 1
        elif s[i] == '.':
            word, i = get_word(s, i + 2)
            while word[len(word) - 1] == ')':
                word = word[:len(word) - 1]
                i -= 1
            continue
        else:
            i += 1
    return stat, funk


def checking_funk(tree, parser, table):
    s = tree.toStringTree(tree, parser)
    i = 0
    word1 = ''
    word2 = ''
    correct = True
    scobki = [True]
    view = [0]
    element = tree
    while i < len(s):
        word1 = word2
        try:
            word2, i = get_word(s, i + 1)
        except:
            return correct
        if word2 in ['(mainClass', '(classDecl', '(methodDecl']:
            while str(type(element.children[view[len(view) - 1]])) not in [classdecl, mainclass, method]:
                view[len(view) - 1] += 1
            element = element.children[view[len(view) - 1]]
            view[len(view) - 1] += 1
            view.append(0)
            scobki.append(True)
            table.enter_scope()
        elif len(word2) > 1 and word2[0] == '(':
            while str(type(element.children[view[len(view) - 1]])) == terminal:
                view[len(view) - 1] += 1
            element = element.children[view[len(view) - 1]]
            view[len(view) - 1] += 1
            view.append(0)
            scobki.append(False)
        while word2 != ')' and word2[len(word2) - 1] == ')':
            word2 = word2[:len(word2) - 1]
            p = scobki.pop(len(scobki) - 1)
            view.pop()
            element = element.parentCtx
            if p:
                table.exit_scope()
        if word2 == '.':
            dot = i
            word, i = get_word(s, i + 1)
            changed = False
            if word[:7] == 'length)' or word == 'length':
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    scobki.pop(len(scobki) - 1)
                    view.pop()
                    element = element.parentCtx
                continue
            else:
                j = dot - 4
                scob = 1
                while scob > 0 and not (scob == 1 and s[j] in ['+', '-', '*', '&', '<']):
                    if s[j] == ')' and s[j - 1] != ' ':
                        scob += 1
                    if s[j] == '(' and s[j + 1] != ' ':
                        scob -= 1
                    j -= 1
                while s[j] != '.':
                    j += 1
                if j != dot - 1:
                    word2, j = get_word(s, j + 2)
                    j = j - len(word2) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                    res = table.lookup_class_method(word1, word2)
                    word1 = res.get_type()
                else:
                    j = i - len(word) - 5
                    while s[j] != ' ':
                        j -= 1
                    word1, j = get_word(s, j + 1)
                    word1 = word1[:len(word1) - 1]
                    if word1[0] == ')':
                        j -= (5 + len(word1))
                        while s[j] != ' ':
                            j -= 1
                        word1, j = get_word(s, j + 1)
                    while word1[len(word1) - 1] == ')':
                        word1 = word1[:len(word1) - 1]
                while word[len(word) - 1] == ')':
                    word = word[:len(word) - 1]
                    scobki.pop()
                    view.pop()
                    element = element.parentCtx
                    changed = True
                if changed:
                    continue
                el = table.lookup_class_method(word1, word)
                if el == None:
                    continue
                can = 0
                j = dot - 1
                while s[j] != '(':
                    j += 1
                num = 1
                while num > 0:
                    j += 1
                    if s[j] == ')':
                        num -= 1
                        if num == 1:
                            can += 1
                    elif s[j] == '(':
                        num += 1
                if can != len(el.get_param()):
                    print('ошибка, попытка вызвать функцию "' + word1 + '.' + word + '" принимающую ' + str(len(el.get_param())) + ' параметр (-а,-ов), с ' + str(can) + ' параметрами, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    exit(0)
                    return False
                pos = 3
                tm = None
                if str(type(element.children[4])) != terminal:
                    tm = element.children[4]
                count = 0
                for j in el.get_param():
                    found = False
                    for k in table.get_expression():
                        if i + pos == k[0]:
                            found = True
                            pos += 1
                            val = 1
                            while val > 0:
                                if s[i + pos] == '(':
                                    val += 1
                                elif s[i + pos] == ')':
                                    val -= 1
                                pos += 1
                            pos += 3
                            if j[0] != k[1]:
                                print('ошибка в вызове функции "' + word1 + '.' + word + '" ожидается параметр типа ' + str(j[0]) + ', задан параметр типа ' + str(k[1]) + ', строка ' + str(tm.children[count].symbol.line) + ', столбец ' + str(tm.children[count].symbol.column))
                                exit(0)
                                return False
                            count += 1
                            break
                    if not found:
                        print('неопознанная ошибка в вызове функции "' + word1 + '.' + word + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        exit(0)
                        return False
    return correct


def semantic(tree, parser):
    table = BuildSymbolTable(tree, parser)
    print('\nв таблице есть следующие записи:')
    table.print()
    print('')
    table.go_null()
    exist, type_correct = check_exist(tree, parser, table)
    if exist:
        print('все вызываемые объекты предварительно объявлены')
    else:
        print('ошибка, не все вызываемые объекты предварительно объявлены')
        return
    if type_correct:
        print('все выражения expression корректны')
    else:
        print('ошибка, не все выражения expression корректны')
        return
    table.go_null()
    correct_stat, funk_return = checking_stat(tree, parser, table)
    if correct_stat:
        print('типизация statements корректна')
    else:
        print('ошибка в типизации statements')
        return
    if funk_return:
        print('возвращаемые значения функций корректны')
    else:
        print('ошибка типа возвращаемого значения')
        return
    table.go_null()
    funk_param = checking_funk(tree, parser, table)
    if funk_param:
        print('параметры в функциях корректны')
    else:
        print('ошибка в параметрах вызываемых функций')
        return
    return table


class command:
    def __init__ (self):
        self.position = 0
        self.name = 'main'
        self.vars = []
        self.commands = []
    def add(self, val):
        self.commands.append(val)


class coms:
    def __init__(self):
        self.num = 0
        self.arr = [command()]

    def getarr(self):
        return self.arr[self.num]

    def create_res(self):
        result = ''
        for i in bytecode.arr:
            result += 'Method '
            result += i.name
            result += '\n'
            for j in range(len(i.vars)):
                result += '#' + str(j) + ' = ' + i.vars[j] + ' '
            if len(i.vars) > 0:
                result += '\n'
            for j in i.commands:
                result += j + '\n'
            result += '\n'
        return result

    def fix(self):
        for i in self.arr:
            l = -1
            for j in i.vars:
                l += 1
                for k in range(len(i.commands)):
                    if i.commands[k] == str('istore ' + j):
                        i.commands[k] = str('istore #' + str(l))
                    if i.commands[k] == str('iload ' + j):
                        i.commands[k] = str('iload #' + str(l))


bytecode = coms()


tabl = None
numexp = 0
numvar = 0
isthis = False
myelement = None
know = False
def compile(element):
    global bytecode
    global tabl
    global numexp
    global numvar
    global isthis
    global myelement
    global know
    myelement = element
    ty = str(type(element))
    if ty == goalcontext:
        for i in element.children:
            compile(i)
    elif ty == mainclass:
        tabl.enter_scope()
        bytecode.getarr().vars.append('class.tmp1')
        bytecode.getarr().vars.append('class.tmp2')
        for i in element.children:
            if str(type(i)) == statement:
                compile(i)
        tabl.exit_scope()
        bytecode.getarr().add('stop')
        bytecode.getarr().position += 1
    elif ty == statement:
        ar = element.children
        try:
            p0 = ar[0].symbol.text
        except:
            p0 = None
        try:
            p1 = ar[1].symbol.text
        except:
            p1 = None
        if p0 == '{':
            for i in ar[1:len(ar) - 1]:
                compile(i)
        elif p0 == 'if':
            compile(ar[2])
            pos = bytecode.getarr().position
            bytecode.getarr().add('if_false ')
            bytecode.getarr().position += 1
            compile(ar[4])
            bytecode.getarr().add('goto ')
            end = bytecode.getarr().position
            bytecode.getarr().position += 1
            compile(ar[6])
            end2 = bytecode.getarr().position
            bytecode.getarr().commands[pos] += str(end + 1)
            bytecode.getarr().commands[end] += str(end2)
        elif p0 == 'while':
            pos = bytecode.getarr().position
            compile(ar[2])
            bytecode.getarr().add('if_false ')
            pos1 = bytecode.getarr().position
            bytecode.getarr().position += 1
            compile(ar[4])
            bytecode.getarr().add('goto ' + str(pos))
            bytecode.getarr().position += 1
            pos2 = bytecode.getarr().position
            bytecode.getarr().commands[pos1] += str(pos2)
        elif p0 == 'System.out.println':
            compile(ar[2])
            bytecode.getarr().add('print')
            bytecode.getarr().position += 1
        elif p1 == '=':
            compile(ar[2])
            if (tabl.lookup_class_var(tabl.get_current().get_clas(), p0)):
                if tabl.get_current().static:
                    print('попытка в объявлении статичного метода "' + str(tabl.get_current().get_clas()) + '.' + str(
                        tabl.get_current().get_name()) + '" вызвать нестатичное поле класса "' + tabl.get_current().get_clas() + '.' + p0 + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    know = True
                    exit(0)
                bytecode.getarr().add('istore this.' + ar[0].symbol.text)
                bytecode.getarr().position += 1
                clas = tabl.lookup_class(tabl.get_current().get_clas())
                counter = 0
                for i in clas.get_ids():
                    if i[1] == p0:
                        clas.identyfied[counter] = True
                    counter += 1
            else:
                tabl.lookup_var(p0)
                bytecode.getarr().add('istore ' + ar[0].symbol.text)
                bytecode.getarr().position += 1
                counter = 0
                for i in tabl.get_current().get_ids():
                    if i[1] == p0:
                        tabl.get_current().identyfied[counter] = True
                    counter += 1
        elif ar[3].symbol.text == '=':
            numexp += 2
            curexp = numexp
            compile(ar[4])
            if tabl.get_current().static:
                print('попытка в объявлении статичного метода "' + str(tabl.get_current().get_clas()) + '.' + str(
                    tabl.get_current().get_name()) + '" вызвать нестатичное поле класса "' + tabl.get_current().get_clas() + '.' + p0 + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                know = True
                exit(0)
            bytecode.getarr().add('istore this.' + ar[2].symbol.text)
            bytecode.getarr().position += 1
            counter = 0
            for i in tabl.get_current().parent.get_ids():
                if i[1] == ar[2].symbol.text:
                    tabl.get_current().parent.identyfied[counter] = True
                counter += 1
        else:
            print('неопознанная инструкция, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
            know = True
            exit(0)
    elif ty == expression:
        numexp += 1
        ar = element.children
        j = 0
        try:
            p0 = ar[0].symbol.text
            if p0 == '(':
                j += 1
                p0 = ar[1].symbol.text

        except:
            p0 = 'None'
        try:
            p1 = ar[1 + j].symbol.text
        except:
            p1 = 'None'
        if p0 == 'new':
            clas = tabl.lookup_class(p1)
            for i in clas.get_ids():
                bytecode.getarr().add('iconst 0')
                bytecode.getarr().position += 1
        elif tabl.lookup_class(p0):
            return
        elif tabl.lookup_class_var(tabl.get_current().get_clas(), p0):
            bytecode.getarr().add('iload this.' + p0)
            bytecode.getarr().position += 1
        elif p0 == 'this':
            isthis = True
            w = tabl.get_current().get_clas()
            clas = tabl.lookup_class(w)
            for i in clas.get_ids():
                bytecode.getarr().add('iload this.' + i[1])
                bytecode.getarr().position += 1
        elif p1 == '+':
            compile(ar[0])
            compile(ar[2])
            bytecode.getarr().add('iadd')
            bytecode.getarr().position += 1
        elif p1 == '-':
            compile(ar[0])
            compile(ar[2])
            bytecode.getarr().add('isub')
            bytecode.getarr().position += 1
        elif p1 == '*':
            compile(ar[0])
            compile(ar[2])
            bytecode.getarr().add('imul')
            bytecode.getarr().position += 1
        elif p1 == '&&':
            compile(ar[0])
            compile(ar[2])
            bytecode.getarr().add('iand')
            bytecode.getarr().position += 1
        elif p1 == '<':
            compile(ar[0])
            compile(ar[2])
            bytecode.getarr().add('ilt')
            bytecode.getarr().position += 1
        elif p0[0] in ['0','1','2','3','4','5','6','7','8','9']:
            bytecode.getarr().add('iconst ' + ar[0].symbol.text)
            bytecode.getarr().position += 1
        elif p0 == 'true':
            bytecode.getarr().add('iconst 1')
            bytecode.getarr().position += 1
        elif p0 == 'false':
            bytecode.getarr().add('iconst 0')
            bytecode.getarr().position += 1
        elif p0 == '!':
            compile(ar[1])
            bytecode.getarr().add('inot')
            bytecode.getarr().position += 1
        elif p1 == '.':
            word = ar[2].symbol.text
            if word != 'length':
                curexp = numexp
                name = word
                changed = False
                pos = 4
                if len(ar) > 4:
                    w = tabl.get_expression()[curexp][1]
                    if not(tabl.lookup_class_method(w, word).static == True and str(type(ar[0].children[0])) == terminal):
                        compile(ar[0])
                    else:
                        numexp += 1
                    word = w + '.' + word
                    clas = tabl.lookup_class(w)
                    compile(ar[4])
                    while True:
                        try:
                            if ar[pos + 1].symbol.text == ',':
                                pos += 2
                                compile(ar[pos])
                            else:
                                break
                        except:
                            break
                    bytecode.getarr().add('invokevirtual ' + word)
                    bytecode.getarr().position += 1
                    met = tabl.lookup_class_method(w, name)
                    if isthis and tabl.get_current().static and (met.static == False):
                        print ('Попытка вызова нестатичного метода "' + met.get_clas() + '.' + met.get_name() + '" в объявлении статичного метода "' + tabl.get_current().get_clas() + '.' + tabl.get_current().get_name() + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                        know = True
                        exit(0)
                    if met.static == False:
                        if isthis:
                            for i in clas.get_ids():
                                bytecode.getarr().add('istore this.' + i[1])
                                bytecode.getarr().position += 1
                        else:
                            for i in clas.get_ids():
                                bytecode.getarr().add('istore class.tmp1')
                                bytecode.getarr().position += 1
                    if not met.returnthis:
                        isthis = False
                else:
                    compile(ar[0])
                    w = tabl.get_expression()[curexp][1]
                    if str(type(ar[0].children[0])) == terminal:
                        if ar[0].children[0].symbol.text != 'this':
                            print('ошибка, попытка получить поля пустого класса "' + ar[0].children[1].symbol.text + '", строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                            know = True
                            exit(0)
                        else:
                            clas = tabl.lookup_class(w)
                            for i in reversed(clas.get_ids()):
                                bytecode.getarr().add('istore this.' + i[1])
                                bytecode.getarr().position += 1
                            bytecode.getarr().add('iload this' + '.' + word)
                            bytecode.getarr().position += 1
                            counter = 0
                    else:
                        clas = tabl.lookup_class(w)
                        for i in reversed(clas.get_ids()):
                            if i[1] != word:
                                bytecode.getarr().add('istore class.tmp1')
                                bytecode.getarr().position += 1
                            else:
                                bytecode.getarr().add('istore class.tmp2')
                                bytecode.getarr().position += 1
                        bytecode.getarr().add('iload class.tmp2')
                        bytecode.getarr().position += 1
                        counter = 0
        elif p1 == 'None' and p0 != 'None':
            if p0 == 'this':
                return
            bytecode.getarr().add('iload ' + p0)
            bytecode.getarr().position += 1
            counter = 0
            for i in tabl.get_current().get_ids():
                if i[1] == p0 and tabl.get_current().identyfied[counter] == False:
                    print('ошибка, попытка вызвать переменную "' + p0 + '" до присвоения значения, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
                    know = True
                    exit(0)
                counter += 1
        elif p0 == 'None':
            compile(ar[j])
        else:
            print('неопознанная инструкция, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
            know = True
            exit(0)
    elif ty == classdecl:
        tabl.enter_scope()
        for i in element.children:
            if str(type(i)) == method:
                compile(i)
        tabl.exit_scope()
    elif ty == method:
        numvar = 0
        tabl.enter_scope()
        bytecode.arr.append(command())
        bytecode.num += 1
        word = tabl.get_current().get_clas()
        pos = 0
        try:
            bytecode.getarr().name = word + '.' + element.children[2].symbol.text
            pos = 5
        except:
            bytecode.getarr().name = word + '.' + element.children[3].symbol.text
            pos = 6
        arr = []
        if tabl.get_current().static == False:
            clas = tabl.get_current().parent
            for pp in clas.get_ids():
                bytecode.getarr().vars.append('this.' + pp[1])
                arr.append('istore this.' + pp[1])
                bytecode.getarr().position += 1
        try:
            a = element.children[pos - 1].symbol.text
        except:
            while element.children[pos + 1].symbol.text == ',':
                bytecode.getarr().vars.append(element.children[pos].symbol.text)
                arr.append('istore ' + element.children[pos].symbol.text)
                bytecode.getarr().position += 1
                pos += 3
            bytecode.getarr().vars.append(element.children[pos].symbol.text)
            arr.append('istore ' + element.children[pos].symbol.text)
            bytecode.getarr().position += 1
            pos += 1
        arr = reversed(arr)
        for pp in arr:
            bytecode.getarr().add(pp)
        bytecode.getarr().vars.append('class.tmp1')
        bytecode.getarr().vars.append('class.tmp2')
        while pos < len(element.children):
            if str(type(element.children[pos])) != terminal:
                compile(element.children[pos])
            pos += 1
        val = tabl.get_current().get_clas()
        if tabl.get_current().static == False:
            clas = tabl.lookup_class(val)
            for i in reversed(clas.get_ids()):
                bytecode.getarr().add('iload this.' + i[1])
                bytecode.getarr().position += 1
        bytecode.getarr().add('return')
        bytecode.getarr().position += 1
        tabl.exit_scope()
    elif ty == var:
        wh = element.children[0].children[0].symbol.text
        if wh not in ['int', 'boolean']:
            print('ошибка, попытка создать переменную типа отличного от int и boolean, строка ' + str(element.start.line) + ', столбец ' + str(element.start.column))
            know = True
            exit(0)
        clas = element.children[1].symbol.text
        clas = tabl.lookup_var(clas)
        typ = clas[0]
        clas = tabl.lookup_class(typ)
        if clas != None:
            for pp in clas.get_ids():
                bytecode.getarr().vars.append(element.children[1].symbol.text + '.' + pp[1])
        else:
            bytecode.getarr().vars.append(element.children[1].symbol.text)
    return 0


def fix_file():
    fin = open("test.txt", 'r')
    s = fin.read()
    t = s.split()
    s = ' '.join(t)
    fin.close()
    fout = fin = open("my_data.txt", 'w')
    fout.write(s)
    fout.close()

def main():
    #fix_file()
    global myelement
    global know
    input = FileStream("test.txt")
    # построение дерева, проверка корректности строки
    lexer = MiniJavaLexer(input)
    stream = CommonTokenStream(lexer)
    parser = MiniJavaParser(stream)
    tree = parser.goal()
    correct = iscorrect(tree)
    if not correct:
        print('ошибка синтаксиса')
    else:
        print('синтакс корректен')
        disp_tree(tree, parser)
        global tabl
        tabl = semantic(tree, parser)
        tabl.go_null()
        try:
            b_code = compile(tree)
            bytecode.fix()
            res = bytecode.create_res()
            fout = open('class.tjc', 'w')
            fout.write(res)
            fout.close()
            k = 0
            for i in bytecode.arr:
                print('')
                print(i.name)
                print(i.vars)
                k = 0
                for j in i.commands:
                    print(str(k) + ' ' + j)
                    k += 1
        except:
            if not know:
                print('найдены незнакомые инструкции при создании байт-кода, строка ' + str(myelement.start.line) + ', столбец ' + str(myelement.start.column))
    exit(0)


if __name__ == '__main__':
    main()