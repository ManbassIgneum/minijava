grammar MiniJava;

goal :  mainClass classDecl*
       ;
mainClass
  :   'class' ID '{' 'public' 'static' 'void' 'main'
                '(' 'String' '[' ']' ID ')' '{' statement '}' '}'
  ;
classDecl
  :   'class' ID  ('extends' ID)? '{' varDecl* methodDecl* '}'
        ;
typ  : 
  'int' '[' ']'
  |   'boolean'
  |   'int'
  |   ID
        ;

varDecl : typ ID ';'
        ;
methodDecl
  :   'public' 'static'? typ ID '(' (typ ID (',' typ ID)*)? ')'
                '{' varDecl* statement* 'return' expression ';' '}'
        ;

statement
  :   '{' statement* '}'
  |   'if' '(' expression ')' statement 'else' statement
  |   'while' '(' expression ')' statement
  |   'System.out.println' '(' expression ')' ';'
  |   ID '=' expression ';'
  |   ID '[' expression ']' '=' expression ';'
  |   expression '.' ID '=' expression ';'
  
  ;
expression:
        expression '.' ID ('(' ( expression ( ',' expression )* )? ')')?
|       '!' expression
|	expression '[' expression ']'
|       expression ('*') expression
|       expression ('+' | '-') expression
|       expression ('<') expression
|       expression ('&&') expression	
|	I_L
|	'true'
|	'false'
|	ID
|	'this'
|	'new' 'int' '[' expression ']'
|	'new' ID '(' ')'
|	'(' expression ')'  
  ;


ID        :   [a-zA-Z_][a-zA-Z0-9_]*;
I_L       :
          '0'
          | [1-9][0-9]*;
WS        :   [ \t\r\n]+ -> skip ;
COMMENT  
          : (('//' .*? '\r'? '\n') | ('/*' .*? '*/'))-> skip;